import { Pipe, PipeTransform } from '@angular/core';

//Modules
import * as moment from 'moment';

@Pipe({
  name: 'moment',
})
export class MomentPipe implements PipeTransform {
  newDate: string;
  current: string;
  calledJamFuture: string;
  calledJamPass: string;

   constructor(){
   }

  transform(date: string, format: string) {
    
    if(format == "L"){
          this.newDate = moment.utc(date).format('L');
          return this.newDate.replace(/,/gi, '');
    } 
    
    else {
      return moment(date).format(format);
    }

  }
}
