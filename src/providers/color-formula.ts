import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

//Providers
import { serverAddresses } from './server.config';

@Injectable()
export class ColorFormulaProvider {
  server = serverAddresses.api;
  formulas: any[];
  details: any[];

  constructor(public http: HttpClient) {
  }

  getAll(){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/color-formulas/all', { headers })
        .subscribe((formulas: any) => {
          this.formulas = formulas;
          resolve(formulas);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  getByID(colorID){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/color-formulas/id/'  + colorID, { headers })
        .subscribe((formula: any) => {
          resolve(formula);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  create(color){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.post(this.server + '/color-formulas/create',color, { headers })
        .subscribe((formula: any) => {
          resolve(formula);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  update(color){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.post(this.server + '/color-formulas/update',color, { headers })
        .subscribe((formula: any) => {
          resolve(formula);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  deleteColor(colorID){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.delete(this.server + '/color-formulas/delete/' + colorID, { headers })
        .subscribe((formula: any) => {
          resolve(formula);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  addDetail(detail){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.post(this.server + '/color-formulas/details/create',detail, { headers })
        .subscribe((detail: any) => {
          resolve(detail);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  deleteDetail(detailID){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.delete(this.server + '/color-formulas/details/delete/' + detailID, { headers })
        .subscribe((formula: any) => {
          resolve(formula);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  updateDetail(detail){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.post(this.server + '/color-formulas/details/update',detail, { headers })
        .subscribe((formula: any) => {
          resolve(formula);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  getAllDetails(){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/color-formulas/details/all', { headers })
        .subscribe((details: any) => {
          this.details = details;
          resolve(details);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  getDetailByID(detailID){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/color-formulas/details/id/'  + detailID, { headers })
        .subscribe((detail: any) => {
          resolve(detail);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  getDetailsByFormulaID(colorID){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/color-formulas/details/formulaID/'  + colorID, { headers })
        .subscribe((details: any) => {
          resolve(details);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

}
