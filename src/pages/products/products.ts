import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController,NavParams, Events } from 'ionic-angular';

//Providers
import { ProductsProvider } from '../../providers/products';
import { GlobalProvider } from '../../providers/global-provider';

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  products: any;
  filteredProducts: any;
  filteredNames: any;
  selectMode = false;
  showSpinner = true;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public productService: ProductsProvider,
              public globalService: GlobalProvider,
              public events: Events,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    if(this.navParams.get('selectMode')){
      this.selectMode = this.navParams.get('selectMode');
    }
    this.loadProducts();
  }

  loadProducts(refresher?){
    this.productService.getAll().then(products => {
      this.products = products;
      this.initializeProducts();
      this.showSpinner = false;
      console.log(products);
      if (refresher) refresher.complete();
    }, err => {
      this.showSpinner = false;
      if (refresher) refresher.complete();
      this.globalService.showToast("Erreur lors de la requête des produits");
    });
  }

  searchProducts(ev: any) {
    // Reset items back to all of the items
    this.initializeProducts();

    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filteredProducts = this.filteredProducts.filter((product) => {
        if (product.ProductNamestyl.toLowerCase().indexOf(val.toLowerCase()) > -1 || product.productNames_suppliers.toLowerCase().indexOf(val.toLowerCase()) > -1){
          return true;
        }
      })
    }
  }

  initializeProducts(){
    this.filteredProducts = this.products;
  }

  viewProduct(product){
    this.navCtrl.push("ViewProductPage", { product: product });
  }

  productClick(product){
    if(this.selectMode){
      this.viewCtrl.dismiss(product);
    }
    else{
      this.viewProduct(product);
    }
  }
}
