import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ColorsPage } from './colors';

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ColorsPage,
  ],
  imports: [
    IonicPageModule.forChild(ColorsPage),
    PipesModule
  ],
})
export class ColorsPageModule {}
