import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

//Providers
import { ColorFormulaProvider } from '../../providers/color-formula';
import { GlobalProvider } from '../../providers/global-provider';

//Modules
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-colors',
  templateUrl: 'colors.html',
})
export class ColorsPage {
  colors: any;
  filteredColors: any;
  filteredNames: any;
  showSpinner = true;
  searching = false;
  validDates = [];
  invalidDates = [];
  searchTerm: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formulaService: ColorFormulaProvider,
              public globalService: GlobalProvider,
              public events: Events) {

              events.subscribe('colors:reload', () => {
                this.showSpinner = true;
                this.loadColors();
              });
  }

  ionViewDidLoad() {
    this.loadColors();
  }

  loadColors(refresher?){
    this.formulaService.getAll().then(colors => {
      this.colors = colors;
      this.validDates = [];
      this.invalidDates = [];
      for(let i=0; i<this.colors.length; i++){
        if(this.colors[i].Date == null) this.invalidDates.push(this.colors[i]);
        else this.validDates.push(this.colors[i]);
      }
      this.validDates.sort(function (a, b) {
        // This is a comparison function that will result in dates being sorted in
        // DESCENDING order.
        if (a.Date > b.Date) return -1;
        if (a.Date < b.Date) return 1;
        return 0;
      });
      this.colors = this.validDates.concat(this.invalidDates);
      
      this.searchColors(this.searchTerm);
      this.showSpinner = false;
      console.log(colors);
      if (refresher) refresher.complete();
    }, err => {
      this.showSpinner = false;
      if (refresher) refresher.complete();
    });
  }

  updateSearchTerm(ev: any){
    this.searchTerm = ev.target.value;
    this.searchColors(this.searchTerm);
  }

  searchColors(val) {
    // Reset items back to all of the items
    this.filteredColors = this.colors;

    if (val && val.trim() != '') {
      this.searching = true;
      this.filteredColors = this.filteredColors.filter((color) => {
        color.searchTerms = "";
        if(String(color.Color_description).toLowerCase().indexOf(val.toLowerCase()) > -1 || String(color.PO_customer).toLowerCase().indexOf(val.toLowerCase()) > -1 || String(color.Job_ref).indexOf(val.toLowerCase()) > -1 || String(color.Customer_name).toLowerCase().indexOf(val.toLowerCase()) > -1 || String(color.Color_name).toLowerCase().indexOf(val.toLowerCase()) > -1 || String(moment(color.Date).format('L')).indexOf(val.toLowerCase()) > -1){
          if(String(color.Job_ref).indexOf(val.toLowerCase()) > -1) color.searchTerms = color.searchTerms + String(color.Job_ref) + ", ";
          else if(String(color.Customer_name).toLowerCase().indexOf(val.toLowerCase()) > -1) color.searchTerms = color.searchTerms + (color.Customer_name);
          else if(String(color.PO_customer).toLowerCase().indexOf(val.toLowerCase()) > -1) color.searchTerms = color.searchTerms + (color.PO_customer);
          else if(String(color.Color_description).toLowerCase().indexOf(val.toLowerCase()) > -1) color.searchTerms = color.searchTerms + '*Description';
          return true;
        }
        else if (val.toLowerCase() == "actif" && color.Actif == 1){
          color.searchTerms = "Actif";
          return true;
        }
        else if(val.toLowerCase() == "glaze" && color.Glaze == 1){
          color.searchTerms = "Glaze";
          return true;
        }
      })
    }
    else {
      this.searching = false;
    }
  }
  
  viewColor(color){
    this.navCtrl.push("ViewColorPage", { color: color });
  }

  addColor(){
    this.navCtrl.push("AddColorPage");
  }
}
